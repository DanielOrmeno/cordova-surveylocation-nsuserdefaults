/*******************************************************************************************************************
|   File: locationManager.js
|   Proyect: Surveys@Griffith - surveyLocation cordova plugin
|
|   Description: - SurveyLocationManager class (swift). Javascript side of the ios cordova location 
|   plugin for the Surveys@Griffth ionic application. 
|
|   Copyright (c) AppFactory. All rights reserved.
*******************************************************************************************************************/

var exec = require('cordova/exec');

// =====================================     CONSTRUCTOR          ===============================================//

function surveyLocation() {}
// =====================================     PLUGIN METHODS      ===============================================//

/********************************************************************************************************************
METHOD NAME: startLocationUpdates
INPUT PARAMETERS: None - empty args
RETURNS: None
    
OBSERVATIONS: Starts location updates if device is authorized
********************************************************************************************************************/
surveyLocation.prototype.startLocationServices = function() {
    exec(function(result){
        // result handler
        console.log(result)
    },
         function(error){
        // error handler
        console.log("Error" + error);
    },
         "surveyLocation",
         "startLocationServices",
        []);
}


/********************************************************************************************************************
METHOD NAME: stopLocationUpdates
INPUT PARAMETERS: command: None - empty args
RETURNS: None
    
OBSERVATIONS: stops location updates if already running
********************************************************************************************************************/
surveyLocation.prototype.stopLocationServices = function() {
    exec(function(result){
        // result handler
        console.log(result)
    },
         function(error){
        // error handler
        console.log("Error" + error);
    },
         "surveyLocation",
         "stopLocationServices",
        []);
}

/********************************************************************************************************************
METHOD NAME: getLocationRecords
INPUT PARAMETERS: command: None - empty args
RETURNS: None
    
OBSERVATIONS: retrieves location records from memory to be handled by javascript
********************************************************************************************************************/
surveyLocation.prototype.getLocationRecords = function(callback) {
    exec(function(data){
        // result handler

        //- New Array for storing JSON Objects
        var JSONlocations= [];
        
         for (i=0; i< data.length; i++) {
            var jsonObj = JSON.parse(data[i]);
            JSONlocations[i] = jsonObj;
            if(i === data.length-1)
                callback(JSONlocations);
         }
         
         for (i=0; i<JSONlocations.length; i++){
            console.log(JSONlocations[i]);
         }
    },
         function(error){
        // error handler
        console.log("Error" + error);
    },
         "surveyLocation",
         "getLocationRecords",
        []);
}

/********************************************************************************************************************
METHOD NAME: getFormattedLocationRecords
INPUT PARAMETERS: command: None - empty args
RETURNS: None
    
OBSERVATIONS: retrieves location records from memory for usability, records are returned as an array of strings
********************************************************************************************************************/
surveyLocation.prototype.getFormattedLocationRecords = function(callback) {
    exec(function(data){
        // result handler

        //- New Array for storing JSON Objects
        var locations= [];
         
         for (i=0; i<data.length; i++){
            console.log(data[i]);
            if(i === data.length-1)
                callback(data);
         }
    },
         function(error){
        // error handler
        console.log("Error" + error);
    },
         "surveyLocation",
         "getLocationRecords",
        []);
}


var SurveyLoc = new surveyLocation();
module.exports = SurveyLoc
